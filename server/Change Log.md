------------------------------------------
         CHANGELOG DEL SERVIDOR           
            Testing Version                         
------------------------------------------


- 1.0.0

 * 26/04/2009: Creado el modulo de manejo de mapas. (Parra)
 * 26/04/2009: A�adidas funciones y procedimientos para obtener y guardar datos. (Parra)
 * 27/04/2009: Nueva remodelacion de frmMain (consola, comandos, AObot). (Parra)
 * 27/04/2009: Sockets y manejo de las conexiones implementados. (Parra)
 * 27/04/2009: Protocolo binario implementado. (Parra)
 * 28/05/2009: Creado modulo de usuarios, hecha la tabla hash para los usuarios _
			y terminada la conexi�n y el manejo de la tabla entre usuarios y sockets. (Parra)