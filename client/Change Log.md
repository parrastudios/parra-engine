------------------------------------------
          CHANGELOG DEL CLIENTE          
             Testing Version              
------------------------------------------


- 1.0.0

 * 28/05/2009: Sockets implementados.  (Parra)
 * 28/05/2009: Nuevo protocolo binario. (Parra)
 * 28/05/2009: Nueva conexi�n completa. (Parra)
 * 28/05/2009: A�adidas funciones principales del cliente. (Parra)
 * 28/05/2009: Creado bucle principal y estructurado. (Parra)
 * 29/05/2009: Motor base y manejo de fuentes. (Parra)
 * 29/05/2009: Lectura de grh (indexaciones). (Parra)
 * 01/06/2009: Motor base terminado y a�adida la carga de texturas. (Parra)
 * 03/06/2009: Iniciaci�n de DirectX terminada y testeada. (Parra)
 * 04/06/2009: Motor de sonido Mp3 din�mico multi-buffer terminado, usa una clase como Loader. (Parra)
 * 07/06/2009: Procedimientos para renderizar implementados, ahora podemos renderizar en diferentes vistas. (Parra)
 * 10/06/2009: OpenGL beta implementado (seguramente lo quite.. no me gusta). (Parra)
 * 12/06/2009: OpenGL beta desimplementado (no me gustaba jaja). (Parra)
 * 13/06/2009: Nueva funci�n para calcular los cuadrados con diferentes perspectivas. (Parra)
 * 14/06/2009: Motion Blur con factor implementado y funcional. (Parra)
 * 25/06/2009: Particulas. (Parra)
 * 26/06/2009: Estoy harto del history log.. no lo voy a continuar. (Parra)